---
templateKey: blog-post
id: 17-Jun-2021
title: Check if the product is existing or not by SKU in Magento 2.
slug: /check-if-product-is-exist-or-not-by-sku-in-magento-2
date: 2021-06-17 11:48:03
description: How to check the product is exist in Magento 2 by SKU. For Simple product, Configurable product, Grouped product, Virtual product, Bundle product, Downloadable product, Gift card, and custom product type.

tags:
  - magento2
  - Product
---

<p>There are 3 ways to check, the product exists or not in Magento 2 by SKU.</p>

1. [Using Product ResourceModel](#1-using-product-resourcemodel)
2. [Using Product Model](#2-using-product-model)
3. [Using ProductRepositoryInterface](#3-using-productrepositoryinterface)

## 1. Using Product ResourceModel:
Using Product ResourceModel get ProductID by SKU. If a product exists then return the Product ID otherwise it will be false.
<br>

```
<?php
/**
 * @Copyright Copyright ©  All rights reserved.
 * @author    Hitesh Vaghasiya <h.g.vaghasiya83@gmail.com>
 */
declare(strict_types=1);

namespace Hitesh\Vaghasiya\Helper;

use Magento\Catalog\Model\ResourceModel\ProductFactory;

/**
 * Class CheckProductBySKU
 * @package Hitesh\Vaghasiya\Helper
 */
class CheckProductBySKU
{
    /**
     * @var ProductFactory
     */
    protected $_productResourceModel;

    /**
     * CheckProductBySKU constructor.
     * @param ProductFactory $productResourceModel
     */
    public function __construct(
        ProductFactory $productResourceModel
    ) {
        $this->_productResourceModel = $productResourceModel;
    }

    /**
     * @param $sku
     * @return false|int
     */
    public function isProductExist($sku)
    {
        return $this->_productResourceModel->create()->getIdBySku($sku);
    }
}
```

Call CheckProductBySKU using constructor in any template file.

```
    $sku = 'SKU-100';
    
    if ($this->checkProductBySKU->isProductExist($sku)) {
        // Product is exist in Magento.
    } else {
        // No Product is exist in Magento.
    }
```
<br>

## 2. Using Product Model:

```
<?php
/**
 * @Copyright Copyright ©  All rights reserved.
 * @author    Hitesh Vaghasiya <h.g.vaghasiya83@gmail.com>
 */
declare(strict_types=1);

namespace Hitesh\Vaghasiya\Helper;

use Magento\Catalog\Model\ProductFactory;

/**
 * Class CheckProductBySKU
 * @package Hitesh\Vaghasiya\Helper
 */
class CheckProductBySKU
{
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * CheckProductBySKU constructor.
     * @param ProductFactory $productFactory
     */
    public function __construct(
        ProductFactory $productFactory
    ) {
        $this->_productFactory = $productFactory;
    }

    /**
     * @param $sku
     * @return int
     */
    public function isProductExist($sku): int
    {
        return $this->_productFactory->create()->getIdBySku($sku);
    }
}

```

Call CheckProductBySKU using constructor in any template file.

```
   $sku = 'SKU-100';
    
    if ($this->checkProductBySKU->isProductExist($sku)) {
        // Product is exist in Magento.
    } else {
        // No Product is exist in Magento.
    }
```
<br>

## 3. Using ProductRepositoryInterface:
```
<?php
/**
 * @Copyright Copyright ©  All rights reserved.
 * @author    Hitesh Vaghasiya <h.g.vaghasiya83@gmail.com>
 */
declare(strict_types=1);

namespace Hitesh\Vaghasiya\Helper;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class CheckProductBySKU
 * @package Hitesh\Vaghasiya\Helper
 */
class CheckProductBySKU
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * CheckProductBySKU constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository
    ) {
        $this->_productRepository = $productRepository;
    }

    /**
     * @param $sku
     * @return ProductInterface
     * @throws Exception
     */
    public function isProductExist($sku): ProductInterface
    {
        try {
            return $this->_productRepository->get($sku);
        } catch (NoSuchEntityException $e) {
            throw new Exception($e->getMessage());
        }
    }
}
```

Call CheckProductBySKU using constructor in any template file.

```
    $sku = "SKU-100";
    try {
        $product = $this->checkProductBySKU->isProductExist($sku);
    } catch (\Exception $exception) {
        echo $exception->getMessage();
    }
```
