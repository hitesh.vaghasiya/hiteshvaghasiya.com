---
templateKey: blog-post
id: 01-may-2019
title: How to install magento2.3 in windows XAMPP
slug: /how-to-install-magento2.3-in-windows-xampp
date: 2019-05-01 08:48:03
description: Easy steps too install Magento2.3 in windows7 or windows8 or windows10 with XAMPP setup. 
headerImage: '/assets/post_image/01-may-2019/magento_windows_xampp.png'
tags:
  - magento
  - magento2.3
  - install
  - windows
  - xampp
---

Easy 2 steps to install Magento2.3 on Windows.
##Steps
1. Install XAMPP
2. Install Magento2.3

### Install XAMPP into Windows

First of all, you have to download XAMPP for the windows from below link.

`<link>` : <https://www.apachefriends.org/download.html>

> <b>Note</b>: Here we have setup the Magento2.3 so we will select the "7.2.17 / PHP 7.2.17" XAMPP version. Based on Magento setup you have to select the XAMPP version. 

1. Download XAMPP 7.2.17 / PHP 7.2.17 version. <div class="postContentImg">
![](/assets/post_image/01-may-2019/img_20190505.png "Download XAMPP for windows") </div>
2. For install XAMPP the setup double click on download. You will see below screenshot then click on NEXT button. <div class="postContentImg">
![](/assets/post_image/01-may-2019/1.png "Install XAMPP for windows") </div>
3. Next step is what for the Server, Program Languages, MySQL. I recommend all option for installation Then clicks on the NEXT button. <div class="postContentImg">
![](/assets/post_image/01-may-2019/2.png "Server, Program Languages") </div>
4. Select the path where you have to install XAMPP. I recommended to install XAMPP on your windows system C: drive. <div class="postContentImg">
![](/assets/post_image/01-may-2019/3.png "Select the path") </div>
5. Click on the next button. <div class="postContentImg">
![](/assets/post_image/01-may-2019/4.png "Next button") </div>
<br> <b>Note:</b> If you have installed on your system you will show below screen otherwise you will see next the screenshot. <div class="postContentImg">
![](/assets/post_image/01-may-2019/5.png "Next button") </div>
6. Now the installation started. <div class="postContentImg">
![](/assets/post_image/01-may-2019/6.png "Installation Process.") </div>
7. Click on finish option. Here is one checkbox for the if you have to show Control Panel. <div class="postContentImg">
![](/assets/post_image/01-may-2019/7.png "Finish Process.") </div>
<br> <b>Note:</b> Security Alert, I recommended the please check both checkboxes for development. <div class="postContentImg">
![](/assets/post_image/01-may-2019/8.png "Windows Security Alert.") </div>
8. The last step for installation is to select the language.XAMPP is provided only 2 languages. <div class="postContentImg">
![](/assets/post_image/01-may-2019/9.png "XAMPP language.") </div>
9. Now you can see the XAMPP Control Panel in your screen.If you do not see below screen then on your windows right corner you have double click on XAMPP icon.  <div class="postContentImg">
![](/assets/post_image/01-may-2019/10.png "XAMPP Control Panel.") </div>
<br> <b>Note:</b> Please click on Apache and MySQL Start action, you localhost will start.

### Install Magento2.3 

Extract the Magento2.3 compressed into the root, from where you can access localhost or server.
1. First Step of installation. <div class="postContentImg">
![](/assets/post_image/01-may-2019/13.png "Magento2.3 installation") </div>
2. Click on the next for readiness check. It will check the PHP version, PHP Extension, File Permission.  <div class="postContentImg">
![](/assets/post_image/01-may-2019/14.png "Next Step") </div>
<div class="postContentImg">
![](/assets/post_image/01-may-2019/15.png "Next Step") </div>
3. Create the Database using PHP MyAdmin Or CLI and set the details. <div class="postContentImg">
![](/assets/post_image/01-may-2019/16.png "Magento2.3 Database") </div>
4. You can set the admin address and session save location. <div class="postContentImg">
![](/assets/post_image/01-may-2019/17.png "admin address and session") </div>
5. Setup the store time zone, Currency, Language. <div class="postContentImg">
![](/assets/post_image/01-may-2019/18.png "Setup the store time zone, Currency, Language") </div>
6. Admin credential. <div class="postContentImg">
![](/assets/post_image/01-may-2019/19.png "Admin credential") </div>
7. Now installation started. <div class="postContentImg">
![](/assets/post_image/01-may-2019/20.png "installation started") </div>
7. Magento installation is done. <div class="postContentImg">
![](/assets/post_image/01-may-2019/11.png "installation Done") </div>



