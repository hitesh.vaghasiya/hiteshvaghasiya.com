---
templateKey: blog-post
id: 26-july-2020
title: Magento 2 JSON encode or decode
slug: /magento2-json-encode-decode
date: 2020-07-26 08:48:03
description: Magento 2 provided class Magento\Framework\Serialize\Serializer\Json for the JSON encode or decode.

tags:
  - magento2
  - magento2.3
  - json
---

Magento 2 deprecated `Magento\Framework\Json\Helper\Data` but introduces new class `Magento\Framework\Serialize\Serializer\Json` for Json encode or decode.

`Magento\Framework\Serialize\Serializer\Json` have 2 object <br>
 1. `serialize()` for Encode the Data like php function  json_encode <br>
 2. `unserialize()` for Decode the string like php function json_decode 
<br>

```
namespace Hitesh\Vaghasiya\Controller;

class Index extends \Magento\Framework\App\Action\Action
{

   /**
    * @var \Magento\Framework\Serialize\Serializer\Json
    */
   protected $_json;

   /**
    * Index constructor.
    * @param \Magento\Framework\Serialize\Serializer\Json $json
    */
   public function __construct(
       \Magento\Framework\Serialize\Serializer\Json $json
   ) {
       $this->_json = $json;
   }

   /**
    * @param $data
    * @return bool|false|string
    */
   public function getJsonEncode($data)
   {
       return $this->_json->serialize($data); // it's same as like json_encode
   }

   /**
    * @param $data
    * @return array|bool|float|int|mixed|string|null
    */
   public function getJsonDecode($data)
   {
       return $this->_json->unserialize($data); // it's same as like json_decode
   }
}
```

<br>

####Usering ObjectManager
<br>

```
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$jsoneObject = $objectManager->create('Magento\Framework\Serialize\Serializer\Json');

//json_encode
$array = array( "option1" => "data1", "option2" => "data2", ); 
$getJsonEncode = $jsoneObject->serialize($array); // it's same as like json_encode

//json_decode
$string = '{"option1":"data1","option2":"data2"}';
$getJsonDecode = $jsoneObject->unserialize($data); // it's same as like json_decode
```
