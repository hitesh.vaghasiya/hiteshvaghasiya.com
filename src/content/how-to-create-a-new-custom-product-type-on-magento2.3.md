---
templateKey: blog-post
id: 22-june-2019
title: How to create a new custom product type on Magento 2.3
slug: /how-to-create-a-new-custom-product-type-on-magento
date: 2019-06-22 07:48:03
description: Magento have many product type but we can create our custom product type.
headerImage: '/assets/post_image/22-june-2019/custom-product-type.png'
tags:
  - magento
---

##Magento default product type list
- Simple Product
- Virtual Product
- Configurable Product
- Grouped Product
- Downloadable Product
- Bundle Product
- Giftcard Product(Enterprise Edition Only)

##How to create new custom product type.

Here we have consider 
<br><b>Vendor name:</b> HGV
<br><b>Module name:</b> Modulename
<br><b>Product type:</b> NewCustomProductType

File structure for the module

- app/code/HGV/Modulename
  - registration.php
  - etc
      - module.xml
      - product_types.xml
  - Model/Product/Type
      - Newcustomproducttype.php
  - Setup
      - InstallData.php

<br>

app/code/HGV/Modulename/registration.php

    <?php
    \Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'HGV_Modulename',
        __DIR__
    );

app/code/HGV/Modulename/etc/module.xml

    <?xml version="1.0" ?>
    <config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
      <module name="HGV_Modulename" setup_version="1.0.0"/>
    </config>

app/code/HGV/Modulename/etc/product_types.xml

    <?xml version="1.0" ?>
    <config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Catalog:etc/product_types.xsd">
      <type label="NewCustomProductType" modelInstance="HGV\Modulename\Model\Product\Type\Newcustomproducttype" name="newcustomproducttype"/>
    </config>

app/code/HGV/Modulename/Setup/InstallData.php

    <?php


    namespace HGV\Modulename\Setup;

    use Magento\Framework\Setup\InstallDataInterface;
    use Magento\Framework\Setup\ModuleContextInterface;
    use Magento\Framework\Setup\ModuleDataSetupInterface;
    use Magento\Eav\Setup\EavSetup;
    use Magento\Eav\Setup\EavSetupFactory;

    class InstallData implements InstallDataInterface
    {

        private $eavSetupFactory;

        /**
        * Constructor
        *
        * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
        */
        public function __construct(EavSetupFactory $eavSetupFactory)
        {
            $this->eavSetupFactory = $eavSetupFactory;
        }

        /**
        * {@inheritdoc}
        */
        public function install(
            ModuleDataSetupInterface $setup,
            ModuleContextInterface $context
        ) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            // associate these attributes with new product type
            $fieldList = [
                'price',
                'special_price',
                'special_from_date',
                'special_to_date',
                'minimal_price',
                'cost',
                'tier_price',
                'weight',
            ];
            
            // make these attributes applicable to new product type
            foreach ($fieldList as $field) {
                $applyTo = explode(
                    ',',
                    $eavSetup->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field, 'apply_to')
                );
                if (!in_array(\HGV\Modulename\Model\Product\Type\Newcustomproducttype::TYPE_ID, $applyTo)) {
                    $applyTo[] = \HGV\Modulename\Model\Product\Type\Newcustomproducttype::TYPE_ID;
                    $eavSetup->updateAttribute(
                        \Magento\Catalog\Model\Product::ENTITY,
                        $field,
                        'apply_to',
                        implode(',', $applyTo)
                    );
                }
            }
        }
    }

<b>Note:</b>Also, You can use on UpgradeData.php.

app/code/HGV/Modulename/Model/Product/Type/Newcustomproducttype.php

    <?php


    namespace HGV\Modulename\Model\Product\Type;

    class Newcustomproducttype extends \Magento\Catalog\Model\Product\Type\AbstractType
    {

        const TYPE_ID = 'newcustomproducttype';

        /**
        * {@inheritdoc}
        */
        public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
        {
            // method intentionally empty
        }
    }