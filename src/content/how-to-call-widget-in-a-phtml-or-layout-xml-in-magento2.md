---
templateKey: blog-post
id: 25-july-2020
title: How to call widget in a .phtml or Layout XML in Magento 2.
slug: /how-to-call-widget-in-a-phtml-or-layout-xml
date: 2020-07-25 08:48:03
description: Magento 2 provided easy way to call widget in to .phtml or Layout XML file.

tags:
  - magento2
  - magento2.3
  - widget
---



### Example:

<b>Block:</b>  `Hitesh\Vaghasiya\Block\Widget\Link` <br>
<b>Template file:</b> `module\widget\link_block.phtml` <br>
<b>Other Arguments:</b>  <br>
 1. Title <br>
 2. Option1 <br>
 3. Option2 * <br>
 \*Requied Argument must add when called the widget in .phtml or layout XML <br>

## Magento 2 : How to call widget in .phtml

```
<?php echo $this->getLayout()->createBlock("Hitesh\Vaghasiya\Block\Widget\Link")->setTitle("WidgetTitle")->setOption1("Data1")->setOption2("Data2")->setTemplate("module\widget\link_block.phtml")->toHtml(); ?>
```

<br>

## Magento 2 : How to call widget in layout XML

```
<block class="Hitesh\Vaghasiya\Block\Widget\Link" name="demoBlock" template="module\widget\link_block.phtml">
    <action method="setData">
        <argument name="title" xsi:type="string">WidgetTitle</argument>
        <argument name="option1" xsi:type="string">Data1</argument>
        <argument name="option2" xsi:type="string">Data2</argument>
    </action>
</block>
```

<br>

## Magento 2 : How to call widget in CMS

```
{{widget type="Hitesh\Vaghasiya\Block\Widget\Link" title="WidgetTitle" option1="Data1" option2="Data2" template="module\widget\link_block.phtml"}}
```