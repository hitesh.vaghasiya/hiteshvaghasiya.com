---
templateKey: blog-post
id: 06-may-2019
title: Magento 2 Daily Useful CLI Commands List
slug: /magento-2-daily-useful-cli-commands-list
date: 2019-05-06 07:48:03
description: Magento 2 and Magento 2.3 Daily Useful CLI Commands List. 
headerImage: '/assets/post_image/06-may-2019/magento2_cli.png'
tags:
  - magento
  - cli
---


### Cache CLI (Status, Enable, Disable, Clean, Flush)

    php bin/magento cache:clean                              Cleans cache type(s)
    php bin/magento cache:disable                            Disables cache type(s)
    php bin/magento cache:enable                             Enables cache type(s)
    php bin/magento cache:flush                              Flushes cache storage used by cache type(s)
    php bin/magento cache:status                             Checks cache status

<br/>

### Indexer CLI (Status, reindex, info, reset, etc...)

    php bin/magento indexer:reindex                          Reindexes Data
    php bin/magento indexer:info                             Shows allowed Indexers
    php bin/magento indexer:reset                            Resets indexer status to invalid
    php bin/magento indexer:set-dimensions-mode              Set Indexer Dimensions Mode
    php bin/magento indexer:set-mode                         Sets index mode type
    php bin/magento indexer:show-dimensions-mode             Shows Indexer Dimension Mode
    php bin/magento indexer:show-mode                        Shows Index Mode
    php bin/magento indexer:status                           Shows status of Indexer

    ---------------------
    Individual ReIndexing
    ---------------------

    php bin/magento indexer:reindex design_config_grid                       Design Config Grid
    php bin/magento indexer:reindex customer_grid                            Customer Grid
    php bin/magento indexer:reindex catalog_product_flat                     Product Flat Data
    php bin/magento indexer:reindex catalog_category_flat                    Category Flat Data
    php bin/magento indexer:reindex catalog_category_product                 Category Products
    php bin/magento indexer:reindex catalog_product_category                 Product Categories
    php bin/magento indexer:reindex catalogrule_rule                         Catalog Rule Product
    php bin/magento indexer:reindex catalog_product_attribute                Product EAV
    php bin/magento indexer:reindex inventory                                Inventory
    php bin/magento indexer:reindex catalogrule_product                      Catalog Product Rule
    php bin/magento indexer:reindex cataloginventory_stock                   Stock
    php bin/magento indexer:reindex catalog_product_price                    Product Price
    php bin/magento indexer:reindex catalogsearch_fulltext                   Catalog Search

<br/>

### Module CLI (Status, Enable, Disable, Uninstall)

    php bin/magento module:disable MODULE_NAME                          Disables specified modules
    php bin/magento module:enable MODULE_NAME                           Enables specified modules
    php bin/magento module:status                                       Displays status of modules
    php bin/magento module:uninstall MODULE_NAME                        Uninstalls modules installed by composer

<br/>

### Modes CLI (Default, Developer, Production, Maintenance)

    php bin/magento deploy:mode:show                         Displays current application mode.
    php bin/magento deploy:mode:set                          Set application mode.
    php bin/magento deploy:mode:set default                  Set Default mode.
    php bin/magento deploy:mode:set developer                Set Developer mode.
    php bin/magento deploy:mode:set production               Set production mode.

    ----------------
    Maintenance Mode
    ----------------

    php bin/magento maintenance:allow-ips                    Sets maintenance mode exempt IPs
    php bin/magento maintenance:disable                      Disables maintenance mode
    php bin/magento maintenance:enable                       Enables maintenance mode
    php bin/magento maintenance:status                       Displays maintenance mode status
<br/>

### Setup CLI (Upgrade, Static Content Deploy, DI Compile, etc...)

    php bin/magento setup:upgrade                            Upgrades the Magento application, DB data, and schema
    php bin/magento setup:static-content:deploy              Deploys static view files
    php bin/magento setup:static-content:deploy en_GB        Deploys static view files based on language pack, if you have different language you can use like: en_US, it_IT etc..
    php bin/magento setup:di:compile                         Generates DI configuration and all missing classes that can be auto-generated
    php bin/magento setup:backup                             Takes backup of Magento Application code base, media and database
    php bin/magento setup:config:set                         Creates or modifies the deployment configuration
    php bin/magento setup:cron:run                           Runs cron job scheduled for setup application
    php bin/magento setup:db-data:upgrade                    Installs and upgrades data in the DB
    php bin/magento setup:db-declaration:generate-patch      Generate patch and put it in specific folder.
    php bin/magento setup:db-declaration:generate-whitelist  Generate whitelist of tables and columns that are allowed to be edited by declaration installer
    php bin/magento setup:db-schema:upgrade                  Installs and upgrades the DB schema
    php bin/magento setup:db:status                          Checks if DB schema or data requires upgrade
    php bin/magento setup:install                            Installs the Magento application
    php bin/magento setup:performance:generate-fixtures      Generates fixtures
    php bin/magento setup:rollback                           Rolls back Magento Application codebase, media and database
    php bin/magento setup:store-config:set                   Installs the store configuration. Deprecated since 2.2.0. Use config:set instead
    php bin/magento setup:uninstall                          Uninstalls the Magento application
    
<br/>

### Cron CLI (Install, Remove, Run)

    php bin/magento cron:install                             Generates and installs crontab for current user
    php bin/magento cron:remove                              Removes tasks from crontab
    php bin/magento cron:run                                 Runs jobs by schedule
<br/>


### Other CLI

    Magento CLI 2.3.0

    Usage:
      command [options] [arguments]

    Options:
      -h, --help            Display this help message
      -q, --quiet           Do not output any message
      -V, --version         Display this application version
          --ansi            Force ANSI output
          --no-ansi         Disable ANSI output
      -n, --no-interaction  Do not ask any interactive question
      -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

    Available commands:
      help                                     Displays help for a command
      list                                     Lists commands

    Admin
      php bin/magento admin:user:create                        Creates an administrator
      php bin/magento admin:user:unlock                        Unlock Admin Account

    App
      php bin/magento app:config:dump                          Create dump of application
      php bin/magento app:config:import                        Import data from shared configuration files to appropriate data storage
      php bin/magento app:config:status                        Checks if config propagation requires update
    
    Catalog
      php bin/magento catalog:images:resize                    Creates resized product images
      php bin/magento catalog:product:attributes:cleanup       Removes unused product attributes.

    Config
      php bin/magento config:sensitive:set                     Set sensitive configuration values
      php bin/magento config:set                               Change system configuration
      php bin/magento config:show                              Shows configuration value for given path. If path is not specified, all saved values will be shown

    Customer
      php bin/magento customer:hash:upgrade                    Upgrade customer's hash according to the latest algorithm

    Dev
      php bin/magento dev:di:info                              Provides information on Dependency Injection configuration for the Command.
      php bin/magento dev:profiler:disable                     Disable the profiler.
      php bin/magento dev:profiler:enable                      Enable the profiler.
      php bin/magento dev:query-log:disable                    Disable DB query logging
      php bin/magento dev:query-log:enable                     Enable DB query logging
      php bin/magento dev:source-theme:deploy                  Collects and publishes source files for theme.
      php bin/magento dev:template-hints:disable               Disable frontend template hints. A cache flush might be required.
      php bin/magento dev:template-hints:enable                Enable frontend template hints. A cache flush might be required.
      php bin/magento dev:tests:run                            Runs tests
      php bin/magento dev:urn-catalog:generate                 Generates the catalog of URNs to *.xsd mappings for the IDE to highlight xml.
      php bin/magento dev:xml:convert                          Converts XML file using XSL style sheets

    Encryption
      php bin/magento encryption:payment-data:update           Re-encrypts encrypted credit card data with latest encryption cipher.

    i18n
      php bin/magento i18n:collect-phrases                     Discovers phrases in the codebase
      php bin/magento i18n:pack                                Saves language package
      php bin/magento i18n:uninstall                           Uninstalls language packages

    Info
      php bin/magento info:adminuri                            Displays the Magento Admin URI
      php bin/magento info:backups:list                        Prints list of available backup files
      php bin/magento info:currency:list                       Displays the list of available currencies
      php bin/magento info:dependencies:show-framework         Shows number of dependencies on Magento framework
      php bin/magento info:dependencies:show-modules           Shows number of dependencies between modules
      php bin/magento info:dependencies:show-modules-circular  Shows number of circular dependencies between modules
      php bin/magento info:language:list                       Displays the list of available language locales
      php bin/magento info:timezone:list                       Displays the list of available timezones

    MSP
      php bin/magento msp:security:recaptcha:disable           Disable backend reCaptcha
      php bin/magento msp:security:tfa:disable                 Globally disable two factor auth
      php bin/magento msp:security:tfa:providers               List all available providers
      php bin/magento msp:security:tfa:reset                   Reset configuration for one user

    Newrelic
      php bin/magento newrelic:create:deploy-marker            Check the deploy queue for entries and create an appropriate deploy marker.

    Queue
      php bin/magento queue:consumers:list                     List of MessageQueue consumers
      php bin/magento queue:consumers:start                    Start MessageQueue consumer

    Sampledata
      php bin/magento sampledata:deploy                        Deploy sample data modules for composer-based Magento installations
      php bin/magento sampledata:remove                        Remove all sample data packages from composer.json
      php bin/magento sampledata:reset                         Reset all sample data modules for re-installation

    Store
      php bin/magento store:list                               Displays the list of stores
      php bin/magento store:website:list                       Displays the list of websites

    Theme
      php bin/magento theme:uninstall                          Uninstalls theme

    Varnish
      php bin/magento varnish:vcl:generate                     Generates Varnish VCL and echos it to the command line

    Redis Cash
      redis-cli flushall
      redis-cli monitor
      redis-cli ping

<br/>