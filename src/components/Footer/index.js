import React from 'react';

import ExternalLink from '../ExternalLink';
import { config } from '../../../data';

import './index.scss';

const Footer = () => (
  <footer className="footer">
    <div className="container">
      <div className="row">
        <div className="col-sm-12 text-center">
        <a href="/about" className="footerabout">About</a>
          <p className="copyright"><i>
          Copyright {new Date().getFullYear()}&nbsp;@&nbsp;
          <a href="/" className="hitsname" title="Hitesh Vaghasiya">Hitesh Vaghasiya</a>. 
            All Rights Reserved</i>
            
          </p>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
