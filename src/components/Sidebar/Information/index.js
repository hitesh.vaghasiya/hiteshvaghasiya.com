import React from 'react';

import PropTypes from 'prop-types';

import Friend from '../Friend';
import LatestPost from '../LatestPost';
import Certification from '../Certification';
import './index.scss';

// eslint-disable-next-line react/prop-types
const Information = ({ totalCount, posts }) => (
  <div className="d-lg-block information my-2">
    {/* <LatestPost posts={posts} /> */}
    {/* <Friend /> */}
    <Certification />
  </div>
);

Information.propTypes = {
  totalCount: PropTypes.number.isRequired,
  posts: PropTypes.array,
};

Information.defaultProps = {
  posts: [],
};

export default Information;
